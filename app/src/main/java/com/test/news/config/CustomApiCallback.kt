package com.test.news.config

interface CustomApiCallback<T> {
    fun onSuccess(data: T?)
    fun onError(error: String?, code: Int)
}