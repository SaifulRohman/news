package com.test.news.config

enum class UIState {
    LOADING,
    SUCCESS,
    ERROR
}