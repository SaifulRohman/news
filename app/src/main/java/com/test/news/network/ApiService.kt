package com.test.news.network

import com.test.news.model.HeaderArticleRes
import com.test.news.model.HeaderSourceRes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    @GET("everything")
    fun getArticles(
        @Query("q") articleTheme: String?,
        @Query("from") from: String?,
        @Query("to") to: String?,
        @Header("X-Api-Key") apiKey: String?
    ): Call<HeaderArticleRes>

    @GET("everything")
    fun getArticlesTechrunch(
        @Query("domains") domains: String?,
        @Query("from") from: String?,
        @Query("to") to: String?,
        @Header("X-Api-Key") apiKey: String?
    ): Call<HeaderArticleRes>

    @GET("top-headlines/sources")
    fun getAllSourceNews(
        @Header("X-Api-Key") apiKey: String?
    ): Call<HeaderSourceRes>

    @GET("top-headlines/sources")
    fun getSourceByCategory(
        @Query("category") category: String?,
        @Header("X-Api-Key") apiKey: String?
    ):Call<HeaderSourceRes>

}