package com.test.news.model

import com.google.gson.annotations.SerializedName

data class SourceRes(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("category")
    val category: String? = null,
    @SerializedName("language")
    val language: String? = null,
    @SerializedName("country")
    val country: String? = null,
) {
    fun mapToSourceModel() = SourceModel(
        id = id.orEmpty(),
        name = name.orEmpty(),
        description = description.orEmpty(),
        url = url.orEmpty(),
        category = category.orEmpty(),
        language = language.orEmpty(),
        country = country.orEmpty(),
    )
}
