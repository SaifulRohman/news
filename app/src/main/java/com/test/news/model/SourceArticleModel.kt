package com.test.news.model

import java.io.Serializable

data class SourceArticleModel (
    val id: String = "",
    val name: String = ""
): Serializable