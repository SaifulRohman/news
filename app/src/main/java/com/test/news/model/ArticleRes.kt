package com.test.news.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ArticleRes (
    @SerializedName("source")
    @Expose
    val source: SourceArticleModel? = null,
    @SerializedName("author")
    @Expose
    val author: String? = null,
    @SerializedName("title")
    @Expose
    val title: String? = null,
    @SerializedName("description")
    @Expose
    val description: String? = null,
    @SerializedName("url")
    @Expose
    val url: String? = null,
    @SerializedName("urlToImage")
    @Expose
    val urlToImage: String? = null,
    @SerializedName("publishedAt")
    @Expose
    val publishedAt: String? = null,
    @SerializedName("content")
    @Expose
    val content: String? = null
) {
    fun mapToArticleModel() : ArticleModel {
        return ArticleModel(
            source = source ?: SourceArticleModel(),
            title = title.orEmpty(),
            author = author.orEmpty(),
            description = description.orEmpty(),
            urlLinkArticle = url.orEmpty(),
            urlImage = urlToImage.orEmpty(),
            publishedAt = publishedAt.orEmpty(),
            content = content.orEmpty()
        )
    }
}