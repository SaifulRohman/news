package com.test.news.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SourceArticleRes(
    @SerializedName("id")
    @Expose
    val id: String? = null,
    @SerializedName("name")
    @Expose
    val name: String? = null
) {
    fun mapToSourceNewsModel(): SourceArticleModel {
        return SourceArticleModel(
            id = id.orEmpty(),
            name = name.orEmpty()
        )
    }
}
