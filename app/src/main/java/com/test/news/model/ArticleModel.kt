package com.test.news.model

import java.io.Serializable

data class ArticleModel(
    val source: SourceArticleModel = SourceArticleModel(),
    val title: String = "",
    val author: String = "",
    val description: String = "",
    val urlLinkArticle: String = "",
    var urlImage: String = "",
    val publishedAt: String = "",
    val content: String = ""
): Serializable
