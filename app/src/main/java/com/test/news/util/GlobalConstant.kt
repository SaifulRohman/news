package com.test.news.util

class GlobalConstant {
    companion object{
        const val FAIL_CONNECT_SERVER = "Fail Connect Server."
        const val FAILE_CONNECT_SERVER_CODE = 9999

        const val FAIL_TOO_MANY_REQUEST = "Maximum Limit Request, Please upgrade to a paid plan if you need more requests."
        const val FAILE_TOO_MANY_REQUEST_CODE = 429

        const val API_KEY = "b580294d77f7417597989feabe14f9a5"
        const val ID_SOURCE_PARAM = "Id Source"
        const val LINK_ARTICLE_PARAM = "Link Article"
    }
}