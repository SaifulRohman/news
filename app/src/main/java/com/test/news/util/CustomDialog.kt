package com.test.news.util

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.TextView
import com.test.news.R

object CustomDialog {

    fun popUpWarningDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.warning_dialog)
        dialog.setCancelable(false)

        val btnOk = dialog.findViewById<TextView>(R.id.btnOk)
        val txvErroMessage = dialog.findViewById<TextView>(R.id.txvErrorMessage)

        txvErroMessage.text = message

        btnOk.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}