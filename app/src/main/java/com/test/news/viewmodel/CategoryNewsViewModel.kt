package com.test.news.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.news.config.CustomApiCallback
import com.test.news.util.GlobalConstant
import com.test.news.config.UIState
import com.test.news.model.HeaderSourceRes
import com.test.news.model.SourceModel
import com.test.news.usecase.CategoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CategoryNewsViewModel @Inject constructor(
    private val useCase: CategoryUseCase
): ViewModel(){

    var categorieLiveData = MutableLiveData<List<SourceModel>>()
    var categorieLiveState = MutableLiveData<UIState>()
    var categorieLiveMessage = MutableLiveData<Pair<Int?, String?>>()

    fun getAllSourceNews() {
        categorieLiveState.value = UIState.LOADING

        useCase.getAllSourceNews(
            viewModelScope,
            apiKey = GlobalConstant.API_KEY,
            callback = object : CustomApiCallback<HeaderSourceRes> {

                override fun onSuccess(data: HeaderSourceRes?) {
                    categorieLiveData.postValue(data?.mapToHeaderSourceModel()?.sources)
                    categorieLiveState.postValue(UIState.SUCCESS)
                }

                override fun onError(error: String?, code: Int) {
                    categorieLiveMessage.postValue(Pair(code, error.orEmpty()))
                    categorieLiveState.postValue(UIState.ERROR)
                }
            }
        )
    }

    fun getSourceByCategory(category: String) {
        categorieLiveState.value = UIState.LOADING

        useCase.getSourceByCategory(
            viewModelScope,
            category = category,
            apiKey = GlobalConstant.API_KEY,
            callback = object : CustomApiCallback<HeaderSourceRes> {

                override fun onSuccess(data: HeaderSourceRes?) {
                    categorieLiveData.postValue(data?.mapToHeaderSourceModel()?.sources)
                    categorieLiveState.postValue(UIState.SUCCESS)
                }

                override fun onError(error: String?, code: Int) {
                    categorieLiveMessage.postValue(Pair(code, error.orEmpty()))
                    categorieLiveState.postValue(UIState.ERROR)
                }
            }
        )
    }

}