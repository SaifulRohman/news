package com.test.news.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.news.config.CustomApiCallback
import com.test.news.util.GlobalConstant
import com.test.news.config.UIState
import com.test.news.model.ArticleModel
import com.test.news.model.HeaderArticleRes
import com.test.news.usecase.ArticleUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val useCase: ArticleUseCase
) : ViewModel() {

    var categorieLiveData = MutableLiveData<List<ArticleModel>>()
    var categorieLiveState = MutableLiveData<UIState>()
    var categorieLiveMessage = MutableLiveData<Pair<Int?, String?>>()

    fun getArticles(articleTheme: String, dateFrom: String, dateTo: String) {
        categorieLiveState.value = UIState.LOADING

        useCase.getArticles(
            viewModelScope,
            articleTheme = articleTheme,
            dateFrom = dateFrom,
            dateTo = dateTo,
            apiKey = GlobalConstant.API_KEY,
            callback = object : CustomApiCallback<HeaderArticleRes>{

                override fun onSuccess(data: HeaderArticleRes?) {
                    categorieLiveData.postValue(data?.mapToArticleModel()?.article)
                    categorieLiveState.postValue(UIState.SUCCESS)
                }

                override fun onError(error: String?, code: Int) {
                    categorieLiveMessage.postValue(Pair(code ?: 0, error.orEmpty()))
                    categorieLiveState.postValue(UIState.ERROR)
                }
            }
        )
    }

    fun getArticlesTechrunch(domains: String, dateFrom: String, dateTo: String) {
        categorieLiveState.value = UIState.LOADING

        useCase.getArticlesTechrunch(
            viewModelScope,
            domains = domains,
            dateFrom = dateFrom,
            dateTo = dateTo,
            apiKey = GlobalConstant.API_KEY,
            callback = object : CustomApiCallback<HeaderArticleRes>{

                override fun onSuccess(data: HeaderArticleRes?) {
                    categorieLiveData.postValue(data?.mapToArticleModel()?.article)
                    categorieLiveState.postValue(UIState.SUCCESS)
                }

                override fun onError(error: String?, code: Int) {
                    categorieLiveMessage.postValue(Pair(code ?: 0, error.orEmpty()))
                    categorieLiveState.postValue(UIState.ERROR)
                }
            }
        )
    }

}