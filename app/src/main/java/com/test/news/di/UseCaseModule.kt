package com.test.news.di

import com.test.news.network.ApiService
import com.test.news.usecase.ArticleUseCase
import com.test.news.usecase.CategoryUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun provideCategoryUseCase(apiService: ApiService) : CategoryUseCase {
        return CategoryUseCase(apiService)
    }

    @Provides
    @Singleton
    fun provideArticleUseCase(apiService: ApiService) : ArticleUseCase {
        return ArticleUseCase(apiService)
    }

}