package com.test.news.presentation.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.view.Window
import android.widget.CalendarView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.news.R
import com.test.news.util.GlobalConstant
import com.test.news.config.UIState
import com.test.news.databinding.ActivityArticleBinding
import com.test.news.model.ArticleModel
import com.test.news.presentation.adapter.ArticleAdapter
import com.test.news.util.CustomDialog
import com.test.news.viewmodel.ArticleViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArticleActivity : AppCompatActivity(), OnClickListener {

    private val viewModel: ArticleViewModel by viewModels()

    private lateinit var binding: ActivityArticleBinding
    private var idSource = ""

    companion object{
        const val TECHCRUNCH = "techcrunch"
        const val THE_NEXT_WEB = "thenextweb"
        const val TITLE_BAR_ARTICLE = "ARTICLE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        idSource = intent.getStringExtra(GlobalConstant.ID_SOURCE_PARAM).toString()
        onObservable()
        getDataArticle(idSource,binding.edtDateFrom.text.toString(),binding.edtDateTo.toString())

        binding.incldArticle.txvTitleHeaderBar.text = TITLE_BAR_ARTICLE
        binding.incldArticle.imgVBack.setOnClickListener(this)
        binding.imgVDateFrom.setOnClickListener(this)
        binding.imgVDateTo.setOnClickListener(this)
        binding.btnSearcArticle.setOnClickListener(this)
    }

    private fun onObservable() {
        viewModel.categorieLiveState.observe(this){state ->
            if(state.equals(UIState.ERROR)) {
                hideLoading()
                CustomDialog.popUpWarningDialog(
                    this,
                    viewModel.categorieLiveMessage.value?.second.toString()
                )
            }

            if (state.equals(UIState.LOADING)) showLoading()
            if (state.equals(UIState.SUCCESS)) hideLoading()
        }

        viewModel.categorieLiveData.observe(this){data ->
            setListAdapter(data)
        }
    }

    private fun showLoading() {
        binding.prgrsBarArticle.isVisible = true
        binding.rcylrCategory.isVisible = false
    }

    private fun hideLoading() {
        binding.prgrsBarArticle.isVisible = false
        binding.rcylrCategory.isVisible = true
    }

    private fun setListAdapter (data: List<ArticleModel>) {
        val adapter = ArticleAdapter(data, this, onClick = {linkArticle ->
            val intent = Intent(this, ArticleDetailActivity::class.java)
            intent.putExtra(GlobalConstant.LINK_ARTICLE_PARAM, linkArticle)
            startActivity(intent)
        })
        binding.rcylrCategory.layoutManager = LinearLayoutManager(this)
        binding.rcylrCategory.adapter = adapter
    }

    private fun getDataArticle(param: String, dateFrom: String, dateTo: String) {
        if (!param.contains(TECHCRUNCH) && !param.contains(THE_NEXT_WEB)) {
            viewModel.getArticles(param, dateFrom, dateTo)
        } else {
            viewModel.getArticlesTechrunch(param, dateFrom, dateTo)
        }
        binding.edtDateFrom.setText("")
        binding.edtDateTo.setText("")
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.incldArticle.imgVBack -> {
                finish()
            }
            binding.imgVDateFrom -> {
                popUpCalendarDialog(true)
            }
            binding.imgVDateTo -> {
                popUpCalendarDialog(false)
            }
            binding.btnSearcArticle -> {
                getDataArticle(idSource,binding.edtDateFrom.text.toString(),binding.edtDateTo.toString())
            }
        }
    }

    private fun popUpCalendarDialog(isDateFrom: Boolean) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.calendar_dialog)
        dialog.setCancelable(false)

        val btnOk = dialog.findViewById<TextView>(R.id.txvOkCalendar)
        val btnCancle = dialog.findViewById<TextView>(R.id.txvCancleCalendar)
        val calendar = dialog.findViewById<CalendarView>(R.id.clndrDate)

        calendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val date = ("$year-${getValueUnder(month)}-${getValueUnder(dayOfMonth)}")
            if (isDateFrom) {
                binding.edtDateFrom.setText(date)
            } else {
                binding.edtDateTo.setText(date)
            }
        }

        btnOk.setOnClickListener {
            dialog.dismiss()
        }

        btnCancle.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun getValueUnder(value: Int): String = if((value + 1) < 10) "0${(value + 1)}" else "${(value + 1)}"
}