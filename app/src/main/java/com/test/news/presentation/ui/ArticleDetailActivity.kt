package com.test.news.presentation.ui

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.test.news.util.GlobalConstant
import com.test.news.databinding.ActivityArticleDetailBinding

class ArticleDetailActivity : AppCompatActivity() , OnClickListener{

    private lateinit var binding: ActivityArticleDetailBinding
    private var linkArticle = ""

    companion object{
        const val TITLE_BAR_ARTICLE_DETAIL = "DETAIL ARTICLE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        linkArticle = intent.getStringExtra(GlobalConstant.LINK_ARTICLE_PARAM).toString()
        setWebView()
        binding.incldArticleDetail.txvTitleHeaderBar.text = TITLE_BAR_ARTICLE_DETAIL
        binding.incldArticleDetail.imgVBack.setOnClickListener(this)
    }

    private fun setWebView() {
        binding.webVDetailArticle.webViewClient = object : WebViewClient(){

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                showLoading()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideLoading()
            }
        }
        binding.webVDetailArticle.loadUrl(linkArticle)
    }

    private fun showLoading() {
        binding.prgrsBarArticleDetail.isVisible = true
        binding.webVDetailArticle.isVisible = false
    }

    private fun hideLoading() {
        binding.prgrsBarArticleDetail.isVisible = false
        binding.webVDetailArticle.isVisible = true
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.incldArticleDetail.imgVBack -> {
                finish()
            }
        }
    }
}