package com.test.news.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.test.news.databinding.ArticleItemBinding
import com.test.news.model.ArticleModel

class ArticleAdapter(
    val news: List<ArticleModel>,
    val context: Context,
    val onClick: ((String) -> Unit)? = null
): RecyclerView.Adapter<ArticleAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            ArticleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount() = news.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(news[position])
    }

    inner class NewsViewHolder(private val binding: ArticleItemBinding): ViewHolder(binding.root) {
        fun bind(item: ArticleModel) {
            Glide.with(context)
                .load(item.urlImage)
                .into(binding.imgVNews)
            binding.txvTitle.text = item.title
            binding.crdViewNews.setOnClickListener {
                onClick?.invoke(item.urlLinkArticle)
            }
        }
    }

}