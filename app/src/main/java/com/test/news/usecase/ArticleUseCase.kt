package com.test.news.usecase

import com.test.news.config.CustomApiCallback
import com.test.news.util.GlobalConstant
import com.test.news.model.HeaderArticleRes
import com.test.news.network.ApiService
import io.reactivex.rxjava3.exceptions.UndeliverableException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.ConnectException
import javax.inject.Inject

class ArticleUseCase @Inject constructor(
    private val apiService: ApiService
) {

    fun getArticles(
        scope: CoroutineScope,
        articleTheme: String,
        dateFrom: String,
        dateTo: String,
        apiKey :String,
        callback: CustomApiCallback<HeaderArticleRes>
    ) {
        scope.launch(Dispatchers.IO) {
            try {
                val call: Call<HeaderArticleRes> = apiService.getArticles(articleTheme, dateFrom,dateTo, apiKey)
                call.enqueue(object : Callback<HeaderArticleRes> {

                    override fun onResponse(call: Call<HeaderArticleRes>, response: Response<HeaderArticleRes>) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            callback.onSuccess(data)
                        } else if (response.code() == 429) {
                            callback.onError(
                                GlobalConstant.FAIL_TOO_MANY_REQUEST,
                                GlobalConstant.FAILE_TOO_MANY_REQUEST_CODE
                            )
                        } else {
                            callback.onError(
                                GlobalConstant.FAIL_CONNECT_SERVER,
                                GlobalConstant.FAILE_CONNECT_SERVER_CODE
                            )
                        }
                    }

                    override fun onFailure(call: Call<HeaderArticleRes>, t: Throwable) {
                        callback.onError(
                            t.message,
                            GlobalConstant.FAILE_CONNECT_SERVER_CODE
                        )
                    }

                })
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }catch (ex: UndeliverableException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: HttpException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: IOException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: ConnectException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }
        }
    }

    fun getArticlesTechrunch(
        scope: CoroutineScope,
        domains: String,
        dateFrom: String,
        dateTo: String,
        apiKey :String,
        callback: CustomApiCallback<HeaderArticleRes>
    ) {
        scope.launch(Dispatchers.IO) {
            try {
                val call: Call<HeaderArticleRes> = apiService.getArticlesTechrunch(domains, dateFrom,dateTo, apiKey)
                call.enqueue(object : Callback<HeaderArticleRes> {

                    override fun onResponse(call: Call<HeaderArticleRes>, response: Response<HeaderArticleRes>) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            callback.onSuccess(data)
                        } else if (response.code() == 429) {
                            callback.onError(
                                GlobalConstant.FAIL_TOO_MANY_REQUEST,
                                GlobalConstant.FAILE_TOO_MANY_REQUEST_CODE
                            )
                        } else {
                            callback.onError(
                                GlobalConstant.FAIL_CONNECT_SERVER,
                                GlobalConstant.FAILE_CONNECT_SERVER_CODE
                            )
                        }
                    }

                    override fun onFailure(call: Call<HeaderArticleRes>, t: Throwable) {
                        callback.onError(
                            t.message,
                            GlobalConstant.FAILE_CONNECT_SERVER_CODE
                        )
                    }

                })
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }catch (ex: UndeliverableException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: HttpException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: IOException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: ConnectException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }
        }
    }

}